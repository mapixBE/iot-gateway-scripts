#!/bin/sh

# Reset Lora radio
cd /home/pi/lora_gateway
/bin/bash reset_lgw.sh

# Start Packet forwarder
cd /home/pi/packet_forwarder/lora_pkt_fwd/
./lora_pkt_fwd