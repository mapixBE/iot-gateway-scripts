# Iot-Gateway-Scripts

A collection of scripts to use a Raspberry pi 4 with chirpstack and influxdb.

## Run scripts
```
curl https://gitlab.com/mapixBE/iot-gateway-scripts/-/raw/main/setupGateway.sh -o setupGateway.sh
sudo ./setupGateway.sh
```