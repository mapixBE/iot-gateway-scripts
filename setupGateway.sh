#!/bin/sh

# Update and upgrade
apt update
apt upgrade

# Install required packages
apt install -y libffi-dev libssl-dev python3-dev python3 python3-pip git

#Install Docker
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
usermod -aG docker pi

# Install Docker-compose
pip3 install docker-compose
systemctl enable docker

# Get chirpstack, lora_gateway and packet forwarder
git clone https://github.com/brocaar/chirpstack-docker.git
git clone https://github.com/Lora-net/lora_gateway.git
git clone https://github.com/Lora-net/packet_forwarder.git
git clone https://gitlab.com/mapixBE/iot-gateway-scripts.git

# Setup Chirpstack
cd /home/pi/chirpstack-docker/
docker-compose up -d
docker update --restart=unless-stopped chirpstack-docker_chirpstack-application-server_1
docker update --restart=unless-stopped chirpstack-docker_chirpstack-network-server_1
docker update --restart=unless-stopped chirpstack-docker_chirpstack-gateway-bridge_1
docker update --restart=unless-stopped chirpstack-docker_postgresql_1
docker update --restart=unless-stopped chirpstack-docker_redis_1
docker update --restart=unless-stopped chirpstack-docker_mosquitto_1

# Setup Influxdb database
mkdir /home/pi/influx-data
cd /home/pi/influx-data
docker run -d --name=influxdb -p 8086:8086 -v $PWD:/var/lib/influxdb2 influxdb
docker update --restart=unless-stopped influxdb
docker network connect chirpstack-docker_default influxdb

# Build the Lora gateway driver
cd /home/pi/lora_gateway/
make all

# Build the packet forwarder
cd /home/pi/packet_forwarder/
make all

# TODO: Setup SPI, I2C, I2S and PWM
cp /home/pi/iot-gateway-scripts/config.txt /boot/config.txt

# Load correct Config file
mv global_conf.json global_conf.json.bac
cp /home/pi/iot-gateway-scripts/global_conf.json global_conf.json
cp /home/pi/iot-gateway-scripts/local_conf.json local_conf.json
nano local_conf.json

# Change reset procedure
cp reset_lgw.sh reset_lgw.sh.bac
cp /home/pi/iot-gateway-scripts/reset_lgw.sh reset_lgw.sh
cp /home/pi/iot-gateway-scripts/embConfigs.sh embConfigs.sh

chmod +x /home/pi/iot-gateway-scripts/startGateway.sh
cp /home/pi/iot-gateway-scripts/startGateway.service /etc/systemd/system/startGateway.service